package supervisor;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class SupervisorIoT extends AbstractActor {

    private final LoggingAdapter LOG = Logging.getLogger(getContext().getSystem(), this);

    public static Props props() {
        return Props.create(SupervisorIoT.class);
    }

    @Override
    public void preStart() {
        LOG.info("Supervisor IOT started");
    }

    @Override
    public void postStop(){
        LOG.info("Supervisor IOT stopped");
    }

    public Receive createReceive() {
        return receiveBuilder().build();
    }
}
