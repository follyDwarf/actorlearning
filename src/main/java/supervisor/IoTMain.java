package supervisor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import java.io.IOException;

public class IoTMain {
    public static void main(String[] args) throws IOException {
        ActorSystem actorSystem = ActorSystem.create("iot-supervisor");

        try {
            ActorRef supervisorRef = actorSystem.actorOf(SupervisorIoT.props(), "iot-supervisor");
            System.out.println("Supervisor Actor is running hit enter to finish");
            System.in.read();

        } finally {

            actorSystem.terminate();
        }
    }
}
