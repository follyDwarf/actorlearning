package device;

import java.util.Optional;

public class DeviceActor {


    public static class ReadTemperature {}

    public static class RespondTemperature {

        final Optional<Double> currentTemperature;

        public RespondTemperature(Double currentTemperature) {
            this(Optional.of(currentTemperature));
        }

        public RespondTemperature(final Optional<Double> currentTemperature) {
            this.currentTemperature = currentTemperature;
        }

    }
}
